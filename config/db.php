<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=shop-tutorial-yii',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
